echo "Making Latin font..."
bdf2psf ComicMono-16.bdf /usr/share/bdf2psf/standard.equivalents /usr/share/bdf2psf/fontsets/Lat15.256 256 ComicMono-Latin.psf
echo "Making Greek font..."
bdf2psf ComicMono-16.bdf /usr/share/bdf2psf/standard.equivalents /usr/share/bdf2psf/fontsets/Greek.256 256 ComicMono-Greek.psf
echo "Making Cyrillic font..."
bdf2psf ComicMono-16.bdf /usr/share/bdf2psf/standard.equivalents /usr/share/bdf2psf/fontsets/CyrKoi.256 256 ComicMono-Cyrillic.psf
echo "Making LGC font..."
bdf2psf ComicMono-16.bdf /usr/share/bdf2psf/standard.equivalents /usr/share/bdf2psf/fontsets/Lat15.256+/usr/share/bdf2psf/fontsets/CyrKoi.256+/usr/share/bdf2psf/fontsets/Greek.256 512 ComicMono-LGC512.psf

