# Comic Mono bitmap font

This is an 8x16 bitmap font styled after "Comic Sans MS".

It borrows most of its characters' appearances from the bold version of the
original font, while adapting others to fit the 8x16 pixel size, make them
easier to tell apart, or to fix some of what I consider to be design flaws of
the original.

(Also, the pseudographics have been borrowed from the Terminus font.)

The font so far supports: ASCII, ISO8859-1 (Latin), Windows Latin, KOI8-R and
CP1251 (Cyrillic).

Currently it is developed in FontForge using its SFD format and the results
exported to BDF (X11 bitmap font format) and PSF (Linux console font format).
